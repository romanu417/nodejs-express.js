const express = require('express');

const router = express.Router();

const { updateUser, deteleUserById, isUser, saveUser, getUser } = require("../services/user.service");
const { validateParams, schemas } = require("../middlewares/user.middleware");
const { getList } = require('../services/file.service');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(getList());
});

/* GET user info. */
router.get('/:id', function(req, res, next) {
  const user = getUser(getList(), req);
  if(user){
    res.send(user);
  } else {
    res.status(400).send(`User with id: ${req.params.id} not found`);
  }
});

/* POST user info. */
router.post('/', validateParams(schemas.addSchema), function(req, res, next){
  const result = saveUser(req.body);

  if(result){
    res.send(`User "${req.body.name}" is created`);
  } else {
    res.status(400).send('Some error');
  }
});

/* PUT user info. */
router.put('/:id', validateParams(schemas.updateSchema), function (req, res, next){
  const user = isUser(getList(), req);
  const id = req.params.id;

  if(!user){
    res.status(400).send(`User with id: ${id} not found`);
  } else if(updateUser(req)) {
    res.status(200).send(`User with id: ${id} was updated`);
  } else {
    res.status(500).send(`Error saving data`);
  }
});

/* DELETE user. */
router.delete('/:id', function(req, res, next){
  const user = isUser(getList(), req);
  const id = req.params.id;
  
  if(!user){
    res.status(400).send(`User with id: ${id} not found`);
  } else if(deteleUserById(id)) {
    res.status(200).send(`User with id: ${id} deleted`);;
  } else {
    res.status(500).send(`Error saving data`);
  }
});


module.exports = router;
