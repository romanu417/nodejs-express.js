const Joi = require('joi');

const validateParams = (schema) => {
  return (req, res, next) => {
    const result = Joi.validate(req.body, schema);
    if (result.error) {
      return res.status(400).json(result.error);
    }
    if (!req.value) { req.value = {}; }
    req.value['body'] = result.value;
    next();
  }
};

const schemas = {
  addSchema: Joi.object().keys({
    name: Joi.string().alphanum().min(2).max(30).required(),
    health: Joi.number().options({ convert: false }).min(1).integer().positive().required(),
    attack: Joi.number().options({ convert: false }).min(1).integer().positive().required(),
    defense: Joi.number().options({ convert: false }).min(1).integer().positive().required(),
    source: Joi.string().uri().required()
  }),
  updateSchema: Joi.object().keys({
    name: Joi.string().alphanum().min(2).max(30),
    health: Joi.number().options({ convert: false }).min(1).integer().positive(),
    attack: Joi.number().options({ convert: false }).min(1).integer().positive(),
    defense: Joi.number().options({ convert: false }).min(1).integer().positive(),
    source: Joi.string().uri()
  })
};



module.exports = {
  validateParams,
  schemas
}