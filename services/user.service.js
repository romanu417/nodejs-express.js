const { getList, rewriteFile } = require('../services/file.service');
const { updateDate, saveData } = require("../repositories/user.repository");


const isUser = (users, req) => {
  if(Number(req.params.id)){
    return users.some((user) => user._id === req.params.id);
  }
  return null;
}
 
const getUser = (users,req) => {
  if(Number(req.params.id)){
    let user = users.find((user) => user._id === req.params.id);
    if(user){
      return user;
    }
  }
  return null;
};

const saveUser = (user) => {
  if (user) {
    return saveData(user);
  } else {
    return null;
  }
}

const updateUser = (req) => {
  if (req.body) {
    return updateDate(req);
  } else {
    return null;
  }
}

const deteleUserById = (id) => {
    const list = getList().filter((user) => user._id !== id);
    
    rewriteFile(list);

    return true;
}
  
  module.exports = {
    getUser,
    saveUser,
    isUser,
    updateUser,
    deteleUserById
  };