const fs = require("fs");

const getList = () => {
    const fileContent = fs.readFileSync("userlist.json", "utf8");
    const list = JSON.parse(fileContent);
    return list;
}

const rewriteFile = (data) => {
    const encode = JSON.stringify(data);  
    fs.writeFileSync('userlist.json', encode); 
}

module.exports = {
    getList,
    rewriteFile
}