const { getList, rewriteFile } = require('../services/file.service');


const saveData = (data) => {
    const usersList = getList();

    const maxId = usersList.reduce((max, current) =>  Math.max(max, current._id), 0);
    const _id = (maxId + 1).toString();

    const newUser = { _id, ...data };
    
    rewriteFile([...usersList, ...newUser]);

    if(usersList && newUser){
        console.log(`${data} is saved`);
        return true;
    } else {
        return false;
    }
}

const updateDate = (req) => {
    const usersList = getList();

    const updatedUser = req.body;
    const id = req.params.id;

    const newList = usersList.map((user) => {
        if(user._id === id){
            return  {...user, ...updatedUser};
        }
        return user;
      });

    rewriteFile(newList);

    if(newList && updatedUser){
        console.log(`${updatedUser.name} is saved`);
        return true;
    } else {
        return false;
    }
}


module.exports = {
    saveData,
    updateDate
};