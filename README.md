# NodeJS + Express project

Server to view, add, edit and delete users list from json file

## Getting Started

- [Instalation](#instalation)
- [GET](#get)
- [POST](#post)
- [PUT](#put)
- [DELETE](#delete)

## Instalation
```
npm install
```
```
npm run start
```

open http://localhost:3000/


## GET

`/user` - Obtaining an array of all users

`/user/:id` - Get one user by ID



## POST

`/user` - Create a user based on data from the request body

### Example json

```json
{
  "name": "Ryu",
  "health": 45,
  "attack": 5,
  "defense": 3,
  "source": "http://www.site.com/img.gif"
}
```


## PUT

`/user/:id` - User update based on data from request body

### Example json
#### Update all info

```json
{
  "name": "Ryu",
  "health": 45,
  "attack": 5,
  "defense": 3,
  "source": "http://www.site.com/img.gif"
}
```
#### Or just a part
```json
{
  "health": 45,
  "attack": 5,
}
```
#### What data should be sent
`name` - Requires the string, letters 2-30.

`health, attack, defense` - Requires the number, min 1, integer, positive.

`source` - Requires the string value to be a valid [RFC 3986](http://tools.ietf.org/html/rfc3986) URI.


## DELETE

`/user/:id` - видалення одного користувача по ID


## Authors

* **Roman Dukhnytskyi** - *Initial work* - [github](https://github.com/romanu417)


## License

This project is licensed under the MIT License
