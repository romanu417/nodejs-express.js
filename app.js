const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const favicon = require('serve-favicon');
const path = require('path');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const app = express();


app.use(favicon(path.join(__dirname, 'img', 'favicon.ico')));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/user', usersRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).send('404 Not Found');
  });


module.exports = app;
